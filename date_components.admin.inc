<?php
/*
 * Hook implementations and functions used only in the admin area, but called via Drupal hooks
 */

function _date_components_field_formatter_settings_form($field, $display, $form, &$form_state) {
	$settings = (isset($display['settings']) && is_array($display['settings']))? $display['settings'] : array();
	date_components_match_long_date_format($settings);
	$info = field_info_field($field['field_name']);
	$hasEndDate = $items[0]['value2'];
	$element = array();
			
	$plural = $info['cardinality'] > 1 || $info['cardinality'] < 0;
	$repeat = (bool) $info['settings']['repeat'];
	$todate = $info['settings']['todate'];
	$precision = $info['settings']['granularity'];

	$hasToDate = (is_string($todate) && strlen($todate) > 2);
	
	$hasHours = _date_components_has_precision($precision, 'hour');
	
	if ($hasToDate) {
		
		$dateRangeDescrItems = array(
				t("Contextual") . ": " . t("9 - 21 Feb 2019 or 15 Feb - 3 Mar 2021"),
				t("Constant") . ": " . t("9 Feb 2019 - 21 Feb 2019"),
		);

		$timeRangeDescrItems = array(
				t("Natural date/time range") . ": <em>" . "07:00" ."</em> ". t("9 Feb 2019 - ") . "  <em>" . "18:00" . "</em> " . t("21 Feb 2019"),
				t("Daily time range") . ": " . t("9 - 21 Feb 2019") . "  <em> " . t("13:00 - 19:00") ."</em>",
		);
		
		if ($hasHours) {
			$rangeLabel = t('Date and time range options');
		} else {
			$rangeLabel = t('Date range options');
		}
		
		$element['range'] = array(
				'#title' => $rangeLabel,
				'#type' => 'fieldset',
				'#collapsible' => TRUE,
				'#collapsed' => TRUE,
				'#tree' => TRUE,
		);
		
		$element['range']['date'] = array(
				'#title' => t('Date range logic'),
				'#type' => 'select',
				'#default_value' => $settings['range']['date'],
				'#options' => date_components_date_range_options(),
				'#required' => TRUE,
				'#suffix' => '<ul><li>'.implode('</li><li>', $dateRangeDescrItems).'</li></ul>'
		);
		
		if ($hasHours) {
			$element['range']['time'] = array(
					'#title' => t('Time range logic'),
					'#type' => 'select',
					'#default_value' => $settings['range']['time'],
					'#options' => date_components_time_range_options(),
					'#required' => TRUE,
					'#suffix' => '<ul><li>'.implode('</li><li>', $timeRangeDescrItems).'</li></ul>'
			);
		}
		
		$element['range']['to_conjunction'] = array(
				'#title' => t('To conjunction'),
				'#type' => 'textfield',
				'#default_value' => $settings['range']['to_conjunction'],
				'#size' => 8,
				'#maxlength' => 20,
				'#required' => FALSE,
		);
	}
	
	if ($hasHours) {
		$element['time'] = array(
				'#title' => t('Time formatting options'),
				'#type' => 'fieldset',
				'#collapsible' => TRUE,
				'#collapsed' => TRUE,
				'#tree' => TRUE,
		);
		
		$element['time']['position'] = array(
				'#title' => t('Time Position'),
				'#type' => 'select',
				'#options' => date_components_time_position_options(),
				'#default_value' => $settings['time']['position'],
				'#required' => TRUE,
		);

		$element['time']['minutes_seconds_mode'] = array(
				'#title' => t('Minute and second display'),
				'#type' => 'select',
				'#default_value' => $settings['time']['minutes_seconds_mode'],
				'#options' => date_components_minutes_seconds_options($precision),
				'#required' => TRUE,
		);
		
		$element['time']['format'] = array(
				'#title' => t('Time format'),
				'#type' => 'select',
				'#default_value' => $settings['time']['format'],
				'#options' => date_components_time_format_options(),
				'#required' => TRUE,
		);
		
		$element['time']['hour_suffix'] = array(
				'#title' => t('Hour suffix'),
				'#type' => 'textfield',
				'#description' => t('When displaying whole hours, e.g. 20h'),
				'#default_value' => $settings['time']['hour_suffix'],
				'#size' => 4,
		);
		
		$element['time']['component_separator'] = array(
				'#title' => t('Time component separator'),
				'#type' => 'textfield',
				'#default_value' => $settings['time']['component_separator'],
				'#size' => 3,
				'#required' => FALSE,
		);
	}
	
	$element['date'] = array(
			'#title' => t('Date formatting options'),
			'#type' => 'fieldset',
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#tree' => TRUE,
	);
	
	if (_date_components_has_precision($precision, 'day')) {
		$element['date']['dayname_format'] = array(
				'#title' => t('Day name display'),
				'#type' => 'select',
				'#options' => date_components_dayname_format_options(),
				'#default_value' => $settings['date']['dayname_format'],
		);
		
		$element['date']['day_format'] = array(
				'#title' => t('Day of month format'),
				'#type' => 'select',
				'#options' => date_components_day_format_options(),
				'#default_value' => $settings['date']['day_format'],
		);
	}
	if (_date_components_has_precision($precision, 'month')) {
		$element['date']['month_format'] = array(
				'#title' => t('Month format'),
				'#type' => 'select',
				'#default_value' => $settings['date']['month_format'],
				'#options' => date_components_month_format_options(),
				'#required' => TRUE,
		);
	}
	if (_date_components_has_precision($precision, 'year')) {
		$element['date']['year_format'] = array(
				'#title' => t('Year format'),
				'#type' => 'select',
				'#default_value' => $settings['date']['year_format'],
				'#options' => date_components_year_format_options(),
				'#required' => TRUE,
		);
	}
	
	$element['date']['component_separator'] = array(
			'#title' => t('Date component separator'),
			'#type' => 'textfield',
			'#default_value' => $settings['date']['component_separator'],
			'#size' => 3,
			'#required' => FALSE,
	);
	
	$element['date']['order'] = array(
			'#title' => t('Date component order'),
			'#type' => 'select',
			'#default_value' => $settings['date']['order'],
			'#options' => date_components_date_order_options(),
			'#required' => TRUE,
	);
	
	$element['tags'] = array(
			'#title' => t('HTML tags'),
			'#type' => 'fieldset',
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#tree' => TRUE,
	);
	if ($hasToDate) {
		$element['tags']['wrapper'] = array(
				'#title' => t('Wrapper tag for to and from date/times'),
				'#type' => 'select',
				'#default_value' => $settings['tags']['wrapper'],
				'#options' => date_components_tag_options('wrapper'),
		);
	}
	
	if ($hasHours && $hasEndDate) {
		$outerTagLabel = t('Outer tag for each date/time');
	} else if ($hasHours) {
		$outerTagLabel = t('Outer tag for date + time');
	} else if (!$hasHours && $hasEndDate) {
		$outerTagLabel = t('Outer tag for each date');
	} else {
		$outerTagLabel = t('Outer date tag');
	}
	
	$element['tags']['outer'] = array(
			'#title' => $outerTagLabel,
			'#type' => 'select',
			'#default_value' => $settings['tags']['outer'],
			'#options' => date_components_tag_options('outer'),
	);
	
	$element['tags']['date'] = array(
			'#title' => t('Date tag'),
			'#type' => 'select',
			'#default_value' => $settings['tags']['date'],
			'#options' => date_components_tag_options('date'),
			'#required' => TRUE,
	);
	
	if ($hasHours) {
		$element['tags']['time'] = array(
				'#title' => t('Time tag'),
				'#type' => 'select',
				'#default_value' => $settings['tags']['time'],
				'#options' => date_components_tag_options('time'),
				'#required' => TRUE,
		);
	}
	
	$element['tags']['component'] = array(
			'#title' => t('Component tag'),
			'#type' => 'select',
			'#default_value' => $settings['tags']['component'],
			'#options' => date_components_tag_options('component'),
			'#required' => TRUE,
	);
	if ($hasToDate) {
		$element['tags']['conjunction'] = array(
				'#title' => t('To conjunction tag'),
				'#type' => 'select',
				'#default_value' => $settings['tags']['conjunction'],
				'#options' => date_components_tag_options('conjunction'),
		);
	}
	
	return $element;
}

function date_components_match_long_date_format(array &$settings) {
	module_load_include('php', 'date_components','classes/DateComponentsAbstract');
	// Parse default long date format to ascertain best options for new date field instances
	module_load_include('php', 'date_components','classes/DateComponentsDateFormatParser');
	$longFormat = variable_get('date_format_long', 'l, F j, Y - H:i');
	$c = new DateComponentsDateFormatParser($longFormat);
	$longDateSettings = $c->params();
	foreach ($longDateSettings as $key => $value) {
		if (!isset($settings[$key]) || empty($settings[$key])) {
			$settings[$key] = $value;
		}
	}
}

function _date_components_field_formatter_settings_summary($field, $instance, $view_mode,$display_type = NULL) {
	$summaryItems = array();
	
	$display = isset($instance['display'][$view_mode])? $instance['display'][$view_mode] : array();
	$settings = isset($display['settings'])? $display['settings'] : array();

	$info = field_info_field($field['field_name']);
	
	$hasEndDate = false;
	if (isset($info["columns"]['value2']) && is_array($info["columns"]['value2'])) {
		if (isset($info["columns"]['value2']['type'])) {
			switch ($info["columns"]['value2']['type']) {
				case 'date':
				case 'datetime':
					$hasEndDate = true;
					break;
			}
		}
	}
	$precision = $info['settings']['granularity'];

	foreach ($precision as $key => $value) {
		switch ($key) {
			case 'hour':
			case 'minute':
				$hasTime = true;
				break;
		}
	}
	
	$plural = $info['cardinality'] > 1 || $info['cardinality'] < 0;
	
	date_components_match_long_date_format($settings);
	$dayNameOptions = date_components_dayname_format_options();
	$timeFormatOpts = date_components_time_format_options();
	$minSecOptions = date_components_minutes_seconds_options();
	$monthFormatOpts = date_components_month_format_options();
	
	$yearFormatOpts = date_components_year_format_options();
	$dateOrderOpts = date_components_date_order_options();
	
	
	if ($hasEndDate) {
		$options = date_components_date_range_options();
		$summaryItems[] = t('Date range display logic: @value', array('@value' => $options[$settings['range']['date']] ));
		if ($hasTime) {
			$options = date_components_time_range_options();
			$summaryItems[] = t('Time range display logic: @value', array('@value' => $options[$settings['range']['time']] ));
		}
	}
	if (!isset($settings['show_dayname'])) {
		$settings['show_dayname'] = 0;
	}
	if (!isset($settings['time_format'])) {
		$settings['time_format'] = 'x';
	}
	$summaryItems[] = t('Time format: @format', array('@format' => $timeFormatOpts[$settings['time']['format']]));
	if (!isset($settings['minutes_seconds_mode'])) {
		$settings['minutes_seconds_mode'] = 'min_contextual';
	}
	$summaryItems[] = t('Min/sec display: @value', array('@value' => $minSecOptions[$settings['time']['minutes_seconds_mode']] ));
	
	if (!isset($settings['date']['dayname_format'])) {
		$summaryItems[] = t('Day name format: @value', array('@value' => $dayNameOptions[$settings['date']['dayname_format']] ));
	}
	
	if (isset($settings['date']['month_format'])) {
		$summaryItems[] = t('Month format: @format', array('@format' => $monthFormatOpts[$settings['date']['month_format']]));
	}
	if (isset($settings['date']['year_format'])) {
		$summaryItems[] = t('Year format: @format', array('@format' => $yearFormatOpts[$settings['year_format']]));
	}
	if (isset($settings['date']['order'])) {
		$summaryItems[] = t('Date order: @format', array('@format' => $dateOrderOpts[$settings['date']['order']]));
	}
	
	if (isset($settings['tags'])) {
			$opts = array();
		foreach ($settings['tags'] as $key =>$tagName) {
			if (!empty($tagName) && $tagName != '-') {
				$opts[] = $key . ': ' . $tagName;
			}
		}
		$summaryItems[] = t('HTML tags: @format', array('@format' => implode(', ', $opts)));
	}
	
	if (!empty($summaryItems)) {
		return '<ul><li>'.implode('</li><li>', $summaryItems).'</li></ul>';
	}
}

function date_components_tag_options($mode = 'component') {
	$outerOpts =  array(
			'-' => 'no wrapper tag(s)',
	);
	$pTagOpt = array(
			'p' => 'p: paragraph tag'
	);
	$neutralOpts =  array(
			'span' => 'span: neutral inline tag',
			'div' => 'div: neutral block tag',
	);
	$timeOpt = array(
			'time' => 'time: HTML5 datetime tag'
	);
	$opts = array();
	switch ($mode) {
		case 'component':
			$opts = $neutralOpts + $pTagOpt;
			break;
		case 'date':
		case 'time':
			$opts = $timeOpt + $neutralOpts;
			break;
		case 'conjunction':
			$opts = $outerOpts + $neutralOpts;
			break;
		default:
			$opts = $outerOpts + $neutralOpts + $timeOpt + $pTagOpt;
			break;
	}
	return $opts;
}

function date_components_date_range_options() {
	return array(
			'contextual' => t("Contextual"),
			'constant' => t("Full start & end dates/times"),
	);
}

function date_components_time_range_options() {
	return array(
			'natural' => t("Natural date/time range"),
			'daily' => t("Daily time range"),
	);
}

function _date_components_has_precision(array &$precision,$key = NULL) {
	if (array_key_exists($key,$precision)) {
		return is_string($precision[$key]) && strlen($precision[$key]) > 1;
	}
	return false;
}

/*
 * Options
*/

function date_components_time_format_options() {
	return array(
			'x' => 'Do not show time',
			'g' => '12 hour clock with AM/PM',
			'H' => '24 hour clock',
	);
}

function date_components_dayname_format_options() {
	$params = array(
			'@day_abbrev' => strftime('%a'),
			'@day_full' => strftime('%A'),
	);
	return array(
			'x' => t('Do not show day name'),
			'D' => t('Abbreviated day name: @day_abbrev', $params),
			'l' => t('Full dayname: @day_full', $params),
	);
}

function date_components_day_format_options() {
	return  array(
			'dx' => 'No day number',
			'j' => 'Simple day number: ' .date('j'),
			'd' => 'Zeropadded day number: ' .date('d'),
			'jS' => 'Simple day number with English ordinal suffix: ' .date('jS'),
	);
}

function date_components_month_format_options() {
	return  array(
			'n' => 'Month number, ' .date('n'),
			'm' => 'Zeropadded month number, ' .date('m'),
			'M' => 'Abbreviated month name, ' . strftime('%h'),
			'F' => 'Full month name '. strftime('%B'),
	);
}

function date_components_year_format_options() {
	return  array(
			'y' => '2-digit year: '.date('y'),
			'Y' => '4-digit year: '.date('Y'),
			'yx' => '2-digits only displayed if not this year ',
			'Yx' => '4-digits: only displayed if not this year',
	);
}

function date_components_minutes_seconds_options() {
	$params = array(
			'@time_secs' => date('H:i:s'),
			'@time_mins' => date('H:i'),
	);
	return  array(
			'min_sec' => t('Constant minute and second display e.g. @time_secs', $params),
			'min_sec_contextual' => t('Contextual minute and second display, e.g. 12:05 for 12:05:00, but 12:08:37'),
			'min' => t('Constant minute display e.g. @time_mins', $params),
			'min_contextual' => 'Contextual minute display e.g. 11h for 11:00, but 11:15',
			'hours_only' => t('Hours only'),
	);
}

function date_components_date_order_options() {
	return  array(
			'dmy' => 'day, month, year',
			'mdy' => 'month, day, year',
			'ymd' => 'year, month, day',
	);
}

function date_components_time_position_options() {
	return  array(
			'before' => 'Before date',
			'after' => 'After date',
	);
}