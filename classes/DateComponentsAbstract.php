<?php

abstract class DateComponentsAbstract {

	static $hourSuffix = 'h';

	static $timeSeparator = ':';
	
	static $dateSeparator = NULL;

	static $hourCode = 'H';

	static $showTime = false;

	static $minSecMode = 'min_contextual';

	static $dayNameCode = 'l';

	static $showDayName = true;

	static $dayCode = 'j';

	static $dateOrder = 'dmy';

	static $twelveHour = false;

	static $monthCode = 'M';

	static $yearCode = 'Y';
	
	static $dateRangeMode = 'contextual';
	
	static $timeRangeMode = 'natural';

	static $dateRangeSeparator = '-';

	static $timeRangeSeparator = '-';
	
	static $timePosition = 'after';

}
