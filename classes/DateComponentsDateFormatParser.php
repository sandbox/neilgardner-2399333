<?php


class DateComponentsDateFormatParser extends DateComponentsAbstract {

	/*
	 * @var array
	 */
	static $dayNameCodes = array('l','D');

	/*
	 * @var array
	*/
	static $dateNumCodes = array('j','d');

	/*
	 * @var array
	*/
	static $ordinalSuffixCodes = array('S');

	/*
	 * @var array
	*/
	static $monthCodes = array('m','n','M','F');

	/*
	 * @var array
	*/
	static $yearCodes = array('y','Y');

	/*
	 * @var array
	*/
	static $hourCodes = array('H','h','g');
	
	/*
	 * @var array
	*/
	static $meridianCodes = array('a','A');

	/*
	 * @var array
	*/
	static $minuteCodes = array('i');

	/*
	 * @var array
	*/
	static $secondCodes = array('s');

	/*
	 * @var string
	*/
	protected $format = '';

	/*
	 * @param string $format
	 * @return void
	*/
	function __construct($format = NULL) {
		$this->format = $format;
		if (!empty($format)) {
			$this->parse();
		}
	}

	/*
	* @return void
	*/
	function parse() {
		$parts = str_split($this->format);
		$dayParts = array();
		$hasTime = false;
		$isTime = false;
		$showDayName = false;
		$hasTimeSeparator = false;
		foreach ($parts as $char) {
			if (in_array($char,self::$dayNameCodes)) {
				self::$dayNameCode = $char;
				$showDayName = true;
				$isTime = false;
			} else if (in_array($char,self::$dateNumCodes)) {
				self::$dayCode = $char;
				$dayParts[] = 'd';
				$isTime = false;
			} else if (in_array($char,self::$monthCodes)) {
				self::$monthCode = $char;
				$isTime = false;
				$dayParts[] = 'm';
			} else if (in_array($char,self::$yearCodes)) {
				self::$yearCode = $char;
				$isTime = false;
				$dayParts[] = 'y';
			} else if (in_array($char,self::$hourCodes)) {
				self::$hourCode = $char;
				$isTime = true;
				$hasTime = true;
			} else if (preg_match('#[^\w]#',$char) && $isTime && !$hasTimeSeparator) {
				self::$timeSeparator = $char;
				$hasTimeSeparator = true;
			}
			self::$dateOrder = implode('',$dayParts);
			self::$twelveHour = self::$hourCode == 'g';
			self::$showTime = $hasTime;
			self::$showDayName = $showDayName;
		}
	}

	/*
	* @return array
	*/
	function params() {
		$vars = get_class_vars('DateComponentsAbstract');
		$data = array();
		$settingsMap = array(
				'hour_suffix' => 'hourSuffix',
				'time_separator' => 'timeSeparator',
				'time_format' => 'hourCode',
				'day_format' => 'dayCode',
				'dayname_format' => 'dayNameCode',
				'date_order' => 'dateOrder',
				'month_format' => 'monthCode',
				'year_format' => 'yearCode',
				'minutes_seconds_mode' => 'minSecMode',
				'date_range_separator' => 'dateRangeSeparator',
				'time_range_separator' => 'timeRangeSeparator',
		);
		if (!empty($vars)) {
			foreach ($vars as $name => $var) {
				if (method_exists($this,$name)) {
					$key = array_search($name,$settingsMap);
					if (is_string($key)) {
						$data[$key] = $this->{$name}();
					}
				}
			}
		}
		return $data;
	}

	/*
	 * @return string
	*/
	function hourSuffix() { return self::$hourSuffix; }

	/*
	 * @return string
	*/
	function timeSeparator() { return self::$timeSeparator; }

	/*
	 * @return string
	*/
	function hourCode() { return self::$hourCode; }

	/*
	 * @return string
	*/
	function dayCode() { return self::$dayCode; }

	/*
	 * @return string
	*/
	function dayNameCode() { return self::$dayNameCode; }

	/*
	 * @return bool
	*/
	function showDayName() { return self::$showDayName; }

	/*
	 * @return bool
	*/
	function showTime() { return self::$showTime; }

	/*
	 * @return string
	*/
	function dateOrder() { return self::$dateOrder; }

	/*
	 * @return bool
	*/
	function twelveHour() { return self::$twelveHour; }

	/*
	 * @return string
	*/
	function monthCode() { return self::$monthCode; }

	/*
	 * @return string
	*/
	function yearCode() { return self::$yearCode; }

	/*
	 * @return string
	*/
	function minSecMode() { return self::$minSecMode; }

	/*
	 * @return string
	*/
	function dateRangeSeparator() { return self::$dateRangeSeparator; }
	
	/*
	 * @return string
	*/
	function timeRangeSeparator() { return self::$timeRangeSeparator; }

}