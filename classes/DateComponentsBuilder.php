<?php
/*
 * Build date field with one HTML element per date / time component and end date time based field instance settings
 * e.g. 
 <span class="date-wrapper month-08 day-7">
 	<span class="dayname">Sunday</span>
 	<span class="day">11</span>
 	<span class="month">Aug</span>
 	<span class="year">13</span>
 </span>
 <span class="time-wrapper am-time">
 	<span class="hrs">11</span>
 	<span class="suffix"> am</span> 
 </span>
 */

class DateComponentsBuilder extends DateComponentsAbstract {

	/*
	 * @const string
	*/
	const DATE_COMPONENTS_DATE_REGEX = '^\s*(\d\d\d\d)-([01]?\d)-([0123]?\d)?';

	/*
	 * @const int
	*/
	const SECS_IN_DAY = 86400;
	
	/*
	 * @var array
	*/
	static $settings;

	/*
	 * @var array
	*/
	static $tags = array(
			'wrapper' => 'div',
			'outer' => 'time',
			'date' => 'div',
			'time' => 'div',
			'component' => 'span',
			'conjunction' => 'span'
	);
	
	/*
	 * @var string
	*/
	static $toConjunction = '-';
	
	/*
	 * @var int
	* Current timestamp
	*/
	static protected $now;
	
	
	static $langCode = 'en';
	
	/*
	 * @var array
	*/
	protected $items = array();

	/*
	 * @var int
	*/
	private $startTimestamp = 0;

	/*
	 * @var int
	*/
	private $endTimestamp = 0;

	/*
	 * @var bool
	*/
	private $hasEndDateTime = false;

	/*
	 * @var bool
	*/
	private $sameTime = true;
	
	/*
	 * @var bool
	*/
	private $sameDay = true;
	
	/*
	 * @var bool
	*/
	private $sameMonth = true;

	/*
	 * @var bool
	 */
	private $sameYear = true;
	
	/*
	 * @var bool
	*/
	private $hasNoWrappers = false;
	
	/*
	 * @var bool
	*/
	private $hasWrapper = false;
	
	/*
	 * @var bool
	*/
	private $hasOuter = true;
	
	/*
	 * @param array $items
	 * @paran array $settings
	 * @return void
	 */
	function __construct(&$items,array $settings) {
		self::$timeRangeSeparator = variable_get('date_components_time_range_separator','-');
		self::$now = new DateTime();
		$this->setLang();
		if (is_array($items) && !empty($items)) {
			$this->setItems($items);
		}
		if (is_array($settings)) {
			$this->applySettings($settings);
		}
	}
	
	function setItems(&$items) {
		$this->items = $items;
	}
	
	function applySettings(array $settings) {
		self::$settings = $settings;
		self::$hourCode = $this->setting('time/format','H');
		self::$twelveHour = self::$hourCode  == 'g';
		self::$showTime = self::$hourCode != 'x';
		self::$timeSeparator = $this->setting('time/component_separator',':');
		self::$dateSeparator = $this->setting('date/component_separator',NULL);
		if (!empty(self::$dateSeparator)) {
			self::$dateSeparator = trim(self::$dateSeparator);
		}
		self::$dayNameCode = $this->setting('date/dayname_format','l');
		self::$dayCode = $this->setting('date/day_format','j');
		self::$showDayName = self::$dayNameCode != 'x';
		self::$dateOrder = $this->setting('date/order','dmy');
		self::$monthCode = $this->setting('date/month_format','M');
		self::$yearCode = $this->setting('date/year_format','Y');
		self::$minSecMode = $this->setting('time/minutes_seconds_mode','min_contextual');
		self::$hourSuffix = $this->setting('time/hour_suffix','');
		self::$tags = $this->setting('tags',self::$tags);
		self::$dateRangeMode = $this->setting('range/date','contextual');
		self::$timeRangeMode = $this->setting('range/time','natural');
		self::$toConjunction = $this->setting('range/to_conjunction','-');
		self::$timePosition = $this->setting('time/position','after');
		$this->hasOuter = $this->hasTag('outer');
		$this->hasWrapper = $this->hasTag('wrapper');
		$this->hasNoWrappers = (!$this->hasOuter && !$this->hasWrapper);
	}
	
	/*
	 * Translate codes used with PHP date function
	 * to locale-sensitive codes compatible with strftime
	 * @param String $code 
	 */
	function translateCode($code) {
		switch ($code) {
			case 'l':
				return '%A';
			case 'D':
				return '%a';
			case 'F':
				return '%B';
			case 'M':
				return '%b';
		}
		return $code;
	}
	
	function setLang() {
		$language =  language_default();
		if  (strlen($language->language) <= 3) {
			self::$langCode = $language->language;
			$lang = date_components_match_language(self::$langCode);
			setlocale(LC_ALL,$lang);
			$dateComponentsLangSet = true;
		}
	}

	/*
	 * Fetch setting with default parameter
	 * @param string $name
	 * @param mixed $default
	 * return mixed 
	 */
	function setting($key = NULL,$default = NULL) {
		if (strpos($key,'/') > 1) {
			list($groupKey,$subKey) = explode('/', $key);
			if (array_key_exists($groupKey, self::$settings) && is_array(self::$settings[$groupKey]) && isset(self::$settings[$groupKey][$subKey])) {
				return self::$settings[$groupKey][$subKey];
			}
		} else if (array_key_exists($key, self::$settings)) {
			return self::$settings[$key];
		}
		return $default;
	}

	/*
	 * Build Drupal rendar array
	 * @return array
	 */
	function buildRenderArray() {
		$element = array();
		foreach ($this->items as $delta => $item) {
			if (isset($item['value']) &&  $this->validDate($item['value'])) {
				$element[$delta] = array(
						'#markup' => $this->buildDateTime($item)
				);
			}
		}
		return $element;
	}

	/*
	 * @param array $item
	 * @return string
	 */
	function buildDateTime(array $item) {
		$this->startTimestamp = strtotime($item['value']);
		$this->hasEndDateTime = !empty($item['value2']) && $this->validDate($item['value2']);
		$dateTimeAttrs = array('datetime' => $item['value']);
		$dateAttrs = array('datetime' => date('Y-m-d',$this->startTimestamp));
		if ($this->hasEndDateTime) {
			$this->endTimestamp = strtotime($item['value2']);
			$dateTimeAttrsTo = array('datetime' => $item['value2']);
			$dateAttrsTo = array('datetime' => date('Y-m-d',$this->endTimestamp));
			$this->hasEndDateTime = ($this->endTimestamp>0 && $this->endTimestamp > $this->startTimestamp);
			if (self::$showTime) {
				$this->sameTime = date('H:i',$this->startTimestamp) == date('H:i',$this->endTimestamp);
			}
			$this->sameDay = date('Y-m-d',$this->startTimestamp) == date('Y-m-d',$this->endTimestamp);
			$this->sameMonth = date('Y-m',$this->startTimestamp) == date('Y-m',$this->endTimestamp);
			$this->sameYear = date('Y',$this->startTimestamp) == date('Y',$this->endTimestamp);
		}
		$showMonth = ($this->hasEndDateTime  && $this->sameMonth && !$this->sameDay) == false;
		$showYear = ($this->hasEndDateTime && $this->sameYear && !$this->sameDay) == false;
		$timeWrapperClass ='time-wrapper';
		$fromClasses = $this->dateClasses('from');
		if ($this->hasNoWrappers) {
			$this->addDatetimeClasses($fromClasses,$this->startTimestamp);
		}
		$fromDate = $this->buildDate($this->startTimestamp,$showMonth,$showYear);
		$fromHTML = $this->buildTag(self::$tags['date'],$fromDate,$fromClasses,$dateAttrs);
		$toHTML = '';
		$fromTime = '';
		$toTime = '';
		$beforeHTML = '';
		$afterHTML = '';
		
		$conjunctionHTML = '';
		if (!empty(self::$toConjunction)) {
			if ($this->hasTag('conjunction')) {
				$conjunctionHTML = $this->buildTag(self::$tags['conjunction'],self::$toConjunction,array('to-conjunction'));
			} else {
				$conjunctionHTML = self::$toConjunction;
			}
		}
		if (self::$showTime && !$this->sameTime) {
			$time = $this->buildTime('start');
			$fromTimeClasses = array($timeWrapperClass,'from-time',date('a',$this->startTimestamp).'-time');
			if ($this->hasNoWrappers) {
				$fromTimeClasses[] = 'date-components';
			}
			$fromTime = $this->buildTag(self::$tags['time'],$time,$fromTimeClasses);
			if (self::$timeRangeMode == 'natural') {
				if (self::$timePosition == 'before') {
					$fromHTML = $fromTime . $fromHTML;
				} else {
					$fromHTML .= $fromTime;
				}
				
			}
		}
		if ($this->hasEndDateTime && !$this->sameDay) {
			
			$toClasses = $this->dateClasses('to');
			if ($this->hasNoWrappers) {
				$this->addDatetimeClasses($toClasses, $this->endTimestamp);
			}
			if ($this->hasNoWrappers) {
				$toClasses[] = 'date-components';
			}
			$toDate = $this->buildDate($this->endTimestamp);
			$toHTML .= $this->buildTag(self::$tags['date'],$toDate,$toClasses,$dateAttrsTo);
		}
		if (self::$showTime) {
			$toTimeClasses = array($timeWrapperClass,date('a',$this->endTimestamp).'-time');
			if ($this->sameTime) {
				$timeMode = 'both';
			} else {
				$timeMode = 'end';
				$toTimeClasses[] = 'from-time';
			}
			$time = $this->buildTime($timeMode);
			if (!empty($time)) {
				if ($this->hasNoWrappers) {
					$toTimeClasses[] = 'date-components';
				}
				$toTime = $this->buildTag(self::$tags['time'],$time,$toTimeClasses);
				if (self::$timeRangeMode == 'natural') {
					if (self::$timePosition == 'before') {
						$toHTML = $toTime . $toHTML;
					} else {
						$toHTML .= $toTime;
					}
				}
			}
		}
		if ($this->hasOuter) {
			$classes = $this->datetimeWrapperClasses('from');
			$this->addDatetimeClasses($classes,$this->startTimestamp);
			$fromHTML = $this->buildTag(self::$tags['outer'],$fromHTML,$classes,$dateTimeAttrs);
			if ($this->hasEndDateTime) {
				$classes = $this->datetimeWrapperClasses('to');
				$this->addDatetimeClasses($classes,$this->endTimestamp);
				if (!empty($toHTML)) {
					$toHTML = $this->buildTag(self::$tags['outer'],$toHTML,$classes,$dateTimeAttrsTo);
				}
			}
		}
		if (self::$showTime && self::$timeRangeMode == 'daily') {
			$separator = (!empty($fromTime) && !empty($toTime))? $this->buildTag(self::$tags['conjunction'],self::$timeRangeSeparator,array('to-conjunction')) : '';
			$timeHTML = $fromTime . $separator . $toTime;
			if ($this->hasOuter) {
				$classes = array('time-range');
				if (!$this->hasWrapper) {
					$classes[] = 'date-components';
				}
				$timeHTML  = $this->buildTag(self::$tags['outer'],$timeHTML,$classes);
			}
			if (self::$timePosition == 'before') {
				$beforeHTML = $timeHTML;
			} else {
				$afterHTML = $timeHTML;
			}
		}
		if (empty($toHTML)) {
			$conjunctionHTML = '';
		}
		$html = $beforeHTML . $fromHTML . $conjunctionHTML . $toHTML . $afterHTML;
		if ($this->hasWrapper) {
			$classes = array('date-range','date-components');
			$html = $this->buildTag(self::$tags['wrapper'],$html, $classes);
		}
		return $html;
	}
	
	/*
	 * Check if tag has a valid valid and if not omit tag
	 * @return false
	 */
	protected function hasTag($tagType = NULL) {
		$tagName = array_key_exists($tagType,self::$tags)? self::$tags[$tagType] : '-'; 
		return (!empty($tagName) && is_string($tagName) && $tagName != '-');
	}

	/*
	 * @param int $timestamp
	 * @param bool $showMonth
	 * @param bool $showYear
	* @return string
	*/
	private function buildDate($timestamp, $showMonth=true,$showYear=true) {
		// Use strftime to respect localisation settings
		if (self::$dateRangeMode == 'constant') {
			$showMonth = true;
			$showYear = true;
		}
		if (self::$showDayName) {
			$dayNameLabel = strftime($this->translateCode(self::$dayNameCode), $timestamp);
			$dayName = $this->buildTag(self::$tags['component'], $dayNameLabel ,array('dayname','weekday-' . date('N', $timestamp))) . " ";
		} else {
			$dayName = '';
		}
		if (self::$dayCode == 'jS') {
			self::$dayCode = 'j<\s\up>S</\s\up>';
		}
		$day = $this->buildTag(self::$tags['component'], date(self::$dayCode,$timestamp),array('day')). " ";
		if ($showMonth) {
			$monthNameLabel = strftime($this->translateCode(self::$monthCode), $timestamp);
			$month = $this->buildTag(self::$tags['component'], $monthNameLabel, array('month')) . " ";
		} else {
			$month = '';
		}
		if ($showYear) {
			$yearContextual = strpos(self::$yearCode,'x') == 1;
			$yearCode = $yearContextual? substr(self::$yearCode,0, 1) : self::$yearCode; 
			$year = date($yearCode,$timestamp);
			if ($yearContextual) {
				$showYear = $year != self::$now->format('Y');
			}
		} 
		if ($showYear) {
			$year = $this->buildTag(self::$tags['component'], $year , array('year')) . " ";
		} else {
			$year =  '';
		}
		if (!empty(self::$dateSeparator)) {
			$separator = $this->buildTag(self::$tags['conjunction'],self::$dateSeparator,array('separator'));
		} else {
			$separator = '';
		}
		$before = '';
		$after = '';
		switch (self::$dateOrder) {
			case 'mdy':
				$parts = array($month,$day, $year);
				$before = $dayName;
				break;
			case 'ymd':
				$date = array($year , $month, $day);
				$after = $dayName;
				break;
			default:
				$date = array($day, $month, $year);
				$before = $dayName;
				break;
		}
		$parts = array();
		foreach ($date as $value) {
			if (is_string($value) && strlen($value) > 1) {
				$parts[] = $value;
			}
		}
		return $before . implode($separator, $parts) . $after;
	}

	/*
	 * @param string $mode
	 * @return string
	*/
	private function buildTime($mode='both') {
		$time = '';
		$contextual = false;
		switch (self::$minSecMode) {
			case 'min_sec':
				$format = self::$hourCode . self::$timeSeparator . 'i' . self::$timeSeparator . ':s' ;
				break;
			case 'min':
				$format = self::$hourCode . self::$timeSeparator . 'i';
				break;
			case 'hours':
				$format = self::$hourCode;
				break;
			case 'min_contextual':
			case 'min_sec_contextual':
				$contextual = true;
				break;
		}
		if ($mode != 'end') {
			if ($contextual) {
				$startHourSuffix = ($this->hasEndDateTime || self::$twelveHour)? '' : self::$hourSuffix;
				$time = $this->contextualTime($this->startTimestamp,$startHourSuffix);
			} else {
				$time = date($format,$this->startTimestamp);
			}
		}
		if (($this->hasEndDateTime && !$this->sameTime  && $mode != 'start') ) {
			if ($contextual) {
				$endHourSuffix = self::$twelveHour? '' : self::$hourSuffix;
				$toTime =  $this->contextualTime($this->endTimestamp,$endHourSuffix);
			} else {
				$toTime = date($format,$this->endTimestamp);
			}
			//$time = $this->wrapTime($time);
			//$time .= $this->buildTag(self::$tags['component'],self::$timeRangeSeparator,array('separator'));
			$time .= $this->wrapTime($toTime);
		}
		if (self::$twelveHour) {
			$startApm = $mode != 'end'? strftime('%p',$this->startTimestamp) : '';
			$endApm = strftime('%p',$this->endTimestamp);
			$apm = NULL;
			if (($endApm != $startApm || $mode == 'end') || (self::$timeRangeMode == 'natural' && !$this->sameDay)) {
				switch ($mode) {
					case 'start':
						$apm = $startApm;
						break;
					case 'end':
						$apm = $endApm;
						break;
				}
			}
			if (!empty($apm)) {
				$time .= $this->buildTag('span', $apm , array('am-pm','suffix'));
			}
		}
		return trim($time);
	}

	/*
	 * @param string $time
	 * @param string $separator
	 * @return string
	 */
	private function wrapTime($time,$separator=':') {
		$strTime = '';
		preg_match('#(\d+)(('.$separator.')(\d+))?(('.$separator.')(\d+))?(\s*\w+)?#',$time,$match);
		if (count($match)>1) {
			if (strlen($match[1])>0) {
				$strTime .= $this->buildTag(self::$tags['component'],$match[1],array('hrs'));
			}
			if (isset($match[4]) && strlen($match[4])>0) {
				$strTime .= $this->buildTag(self::$tags['component'],$match[3],array('separator'));
				$strTime .= $this->buildTag(self::$tags['component'],$match[4],array('min'));
			}
			if (isset($match[7]) && strlen($match[7])>0) {
				$strTime .= $this->buildTag(self::$tags['component'],$match[6],array('separator'));
				$strTime .= $this->buildTag(self::$tags['component'],$match[7],array('sec'));
			}
			if (isset($match[8]) && strlen($match[8])>0) {
				$strTime .= $this->buildTag(self::$tags['component'],$match[8],array('suffix'));
			}
		}
		if (!empty($strTime)) {
			$time = $strTime;
		}
		return $time;
	}

	/*
	 * @param int $timestamp
	 * @param string $hourSuffix
	 * @return string
	 */
	protected function contextualTime($timestamp = 0, $hourSuffix=NULL) {
		$time = date(self::$hourCode,$timestamp);
		$minutes = (int) date("i",$timestamp);
		if (self::$minSecMode == 'min_sec_contextual') {
			$seconds = (int) date("s",$timestamp);
		} else {
			$seconds = 0;
		}
		$hoursOnly = ($minutes == 0 && $seconds == 0);
		if (!$hoursOnly) {
			$time .= self::$timeSeparator . $minutes;
		}
		if ($seconds > 0) {
			$time .= self::$timeSeparator . $seconds;
		}
		if ($hoursOnly && !empty($hourSuffix)) {
			$time .= $hourSuffix;
		}
		return $time;
	}
	
	/*
	 * @param string $name
	* @param string $value
	* @param array $classes
	* @param array $attrs
	* @return string
	*/
	protected function buildTag($name = 'span',$value=NULL,$classes = array(),$attrs = array()) {
		$name = trim($name);
		if (!empty($classes) && is_array($classes)) {
			$attrs['class'] = $classes;
		}
		return '<' . $name . drupal_attributes($attrs) . '>' . $value . '</' . $name . '>';
	}

	/*
	 * @param string $date
	 * @return bool
	 */
	function validDate($date = NULL) {
		return (bool) preg_match('#'.self::DATE_COMPONENTS_DATE_REGEX.'#', $date);
	}
	
	function isToday($timestamp = 0) {
		return $this->dayDiff($timestamp) == 0;
	}
	
	function dayDiff($timestamp = 0) {
		return floor($timestamp / self::SECS_IN_DAY) - floor(self::$now->getTimestamp() / self::SECS_IN_DAY);
	}
	
	function weekDiff($timestamp) {
		$dayNum = date('N',$timestamp  );
		$ts = $timestamp - (($dayNum-1) * self::SECS_IN_DAY);
		return floor($this->dayDiff($ts) / 7);
	}
	
	function monthDiff($timestamp = 0) {
		return $this->_toMonthYear($timestamp) - $this->_toMonthYear(self::$now->getTimestamp());
	}
	
	function yearDiff($timestamp = 0) {
		return intval(date('Y',$timestamp)) - intval(self::$now->format('Y') );
	}
	
	private function _toMonthYear($timestamp) {
		return ( date('Y',$timestamp) * 12) + (intval(date('n', $timestamp)) - 1); 
	}
	
	private function year($timestamp) {
		return (int) date('Y',$timestamp);
	}
	
	private function addDatetimeClasses(array &$classes,$timestamp = 0) {
		$classes = array_merge($classes, $this->datetimeClasses($timestamp));
	}
	
	private function datetimeClasses($timestamp = 0) {
		$classes = array('month-'.date("m",$this->startTimestamp).'','day-' . date("N",$this->startTimestamp) );
		$diff = $this->dayDiff($timestamp);
		$this->_nextPrevClasses($classes, 'day', $diff, 'yesterday','today','tomorrow');
		if ($diff > 0) {
			$classes[] = 'future';
		} else if ($diff < 0) {
			$classes[] = 'past';
		}
		$classes[] = 'days-offset-' . abs($diff);
		
		$classes[] = 'week-' . date('W', $timestamp);
		$weekDiff = $this->weekDiff($timestamp);
		
		$this->_nextPrevClasses($classes, 'week', $weekDiff);
		
		$classes[] = 'week-offset-' . abs($weekDiff);
		
		$monthDiff = $this->monthDiff($timestamp);
		$this->_nextPrevClasses($classes, 'month', $monthDiff);
		$classes[] = 'month-offset-' . abs($monthDiff);
		$classes[] = 'year-' . date('Y', $timestamp);
		
		$yearDiff = $this->yearDiff($timestamp);
		$this->_nextPrevClasses($classes,'year', $yearDiff);
		
		return $classes;
	}
	
	private function _nextPrevClasses(array &$classes,  $type = 'month',$diff = 0, $prevName=NULL,$thisName=NULL,$nextName = NULL) {
		if (is_array($classes)) {
			if ($diff == 0) {
				if (empty($thisName)) {
					$thisName = 'this-' . $type;
				}
				$classes[] = $thisName;
			} else if ($diff == 1) {
				if (empty($thisName)) {
					$nextName = 'next-' . $type;
				}
				$classes[] = $nextName;
			} else if ($diff == -1) {
				if (empty($prevName)) {
					$prevName = 'prev-' . $type;
				}
				$classes[] = $prevName;
			}
		}
	}
	
	private function datetimeWrapperClasses($type = 'to') {
		$classes = array('datetime', 'lang-' . self::$langCode );
		if ($this->hasEndDateTime && !$this->sameDay) {
			$classes[] = $type . '-datetime';
		}
		if (!$this->hasWrapper) {
			$classes[] = 'date-components';
		}
		return $classes;
	}
	
	private function dateClasses($type = 'to') {
		$classes = array('date');
		if ($this->hasEndDateTime && !$this->sameDay) {
			$classes[] = $type . '-date';
		}
		if (!$this->hasWrapper) {
			$classes[] = 'date-components';
		}
		return $classes;
	}

}