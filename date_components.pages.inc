<?php

/*
 * Functiins used only in field views using this date formatter 
 */

function date_components_build_element(&$items,&$display) {
	$settings = (isset($display['settings']) && is_array($display['settings']))? $display['settings'] : array();
	module_load_include('php', 'date_components','classes/DateComponentsAbstract');
	module_load_include('php', 'date_components','classes/DateComponentsBuilder');
	$dateBuilder = new DateComponentsBuilder($items,$settings);
	$css = variable_get('date_components_css' ,DATE_COMPONENTS_DEFAULT_CSS);
	if (!empty($css) && strlen($css) > 4) {
		drupal_add_css(drupal_get_path('module', 'date_components') . '/css/'.$css.'.css');
	}
	
	return $dateBuilder->buildRenderArray();
}

function date_components_match_setting(&$settings,$key = NULL, $default = NULL) {
	if (is_array($settings) && isset($settings[$key]) && !empty($settings[$key])) {
		return $settings[$key];
	}
	return $default;
}